package main.john.bookQuote;

public class TempConverter {



    public double convertTemp(String celsius){
        Double temp = Double.parseDouble(celsius);
        Double fahrenheit = (double) (temp * (9/5) + 32);
        return fahrenheit;
    }
}
